package com.ax

import com.ax.R
import com.jfinal.core.Controller

fun Controller.renderOk(msg: String = "") {
    this.renderJson(R.ok(msg))
}

fun getCurrentUserId() = 2L