package com.ax

import com.alibaba.fastjson.JSONObject

/**
JSONObject ext getArray
转换为指定类型的数组
 *@param key
 * */
inline fun <reified T> JSONObject.getArray(key: String): Array<T> {
    return this.getJSONArray(key).toArray(arrayOf())
}
