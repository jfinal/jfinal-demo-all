package com.ax

class R {
    //0为成功,1为失败, 其余场景自己扩展
    var code: Int = 0
    var msg: String? = null
    var data: Any? = null


    fun code(code: Int): R {
        this.code = code
        return this
    }

    fun data(data: Any): R {
        this.data = data
        return this
    }

    fun msg(msg: String): R {
        this.msg = msg
        return this
    }

    companion object {
        fun fail(): R {
            val r = R()
            r.code = 1
            return r
        }

        fun fail(msg: String): R {
            val r = R()
            r.code = 1
            r.msg = msg
            return r
        }


        fun msg(msg: String): R {
            val r = R()
            r.msg = msg;
            return r
        }

        fun ok(msg: String): R {
            return msg(msg)
        }

        fun data(data: Any): R {
            val r = R()
            r.data = data
            return r
        }

        fun getMillsToStr(): String {
            return System.currentTimeMillis().toString()
        }

    }
}