package com.ax.framework.jfinal.server


import cn.hutool.core.io.resource.ClassPathResource
import com.alibaba.fastjson.serializer.SerializeConfig
import com.alibaba.fastjson.serializer.ToStringSerializer
import com.ax.framework.jfinal.db.Arp
import com.jfinal.server.undertow.UndertowConfig
import com.jfinal.server.undertow.ssl.SslConfig
import org.apache.logging.log4j.core.config.ConfigurationSource
import org.apache.logging.log4j.core.config.Configurator

/**
 * UndertowConfig
 *
 *
 * 待添加功能
 * 1：ssl
 * 2：http2
 */
class AppUndertowConfig : UndertowConfig {

    val app_prop: AppPropExt

    constructor(jfinalConfigClass: String, apext: AppPropExt) {
        this.app_prop = apext
        this.jfinalConfig = jfinalConfigClass
        init()
    }




    override fun `init`() {
        devMode = app_prop.getBoolean(DEV_MODE, devMode);
        port = app_prop.getInt(PORT, port);
        host = app_prop.get(HOST, host)?.trim();
        contextPath = app_prop.get(CONTEXT_PATH, contextPath)?.trim();
        resourcePath = app_prop.get(RESOURCE_PATH, resourcePath)?.trim();

        ioThreads = app_prop.getInt(IO_THREADS, 1);
        workerThreads = app_prop.getInt(WORKER_THREADS, 16);

        gzipEnable = app_prop.getBoolean(GZIP_ENABLE, gzipEnable);
        gzipLevel = checkGzipLevel(app_prop.getInt(GZIP_LEVEL, gzipLevel));
        gzipMinLength = app_prop.getInt(GZIP_MIN_LENGTH, gzipMinLength);

        http2Enable = app_prop.getBoolean(HTTP2_ENABLE, http2Enable);

        sessionTimeout = app_prop.getInt(SESSION_TIMEOUT, 3000);
        // sessionHotSwap = app_prop.getBoolean(SESSION_HOT_SWAP, sessionHotSwap);

        val get = app_prop.get(HOT_SWAP_CLASS_PREFIX)
        if (get != null) {
            hotSwapClassPrefix = get
        }

        sslConfig = SslConfig(app_prop);
        httpToHttps = app_prop.getBoolean(HTTP_TO_HTTPS, httpToHttps);
        httpDisable = app_prop.getBoolean(HTTP_DISABLE, httpDisable);
    }

}

private val UNDERTOW_CONFIG = "undertow.txt"
private val UNDERTOW_CONFIG_PRO = "undertow-pro.txt"
private val DEV_MODE = "undertow.dev_mode"
private val PORT = "undertow.port"
private val HOST = "undertow.host"
private val CONTEXT_PATH = "undertow.contextPath"
private val RESOURCE_PATH = "undertow.resourcePath"
private val IO_THREADS = "undertow.ioThreads"
private val WORKER_THREADS = "undertow.workerThreads"
private val GZIP_ENABLE = "undertow.gzip.enable"
private val GZIP_LEVEL = "undertow.gzip.level"
private val GZIP_MIN_LENGTH = "undertow.gzip.minLength"
private val HTTP2_ENABLE = "undertow.http2.enable"
private val SESSION_TIMEOUT = "undertow.session.timeout"
private val SESSION_HOT_SWAP = "undertow.session.hotSwap"
private val HOT_SWAP_CLASS_PREFIX = "undertow.hot_swap_class_prefix"
private
// ssl 模式下 http 请求是否跳转到 https
val HTTP_TO_HTTPS = "undertow.http.toHttps"
// ssl 模式下是否关闭 http
private val HTTP_DISABLE = "undertow.http.disable"