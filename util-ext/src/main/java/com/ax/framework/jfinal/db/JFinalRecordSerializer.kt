/**
 * create by zhang_tian_xiao, 2018/8/7 10:29
 *
 * */
package com.ax.framework.jfinal.db

import com.alibaba.fastjson.serializer.JSONSerializer
import com.alibaba.fastjson.serializer.ObjectSerializer
import com.jfinal.plugin.activerecord.CPI
import com.jfinal.plugin.activerecord.Model
import com.jfinal.plugin.activerecord.Record
import java.io.IOException
import java.lang.reflect.Type

class JFinalRecordSerializer internal constructor() : ObjectSerializer {

    @Throws(IOException::class)
    override fun write(serializer: JSONSerializer, `object`: Any?, fieldName: Any?, fieldType: Type?, features: Int) {
        if (`object` != null) {
            val record = `object` as Record?

            val map = record!!.columns
            serializer.write(map)
        }
    }
}