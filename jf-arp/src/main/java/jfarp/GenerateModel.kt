package jfarp

import cn.hutool.core.io.resource.ClassPathResource
import com.ax.framework.jfinal.db.Arp

fun main(args: Array<String>) {

//加载yml文件
    val stream = ClassPathResource("datasource.yml").stream
//初始化数据源
    Arp.generic(stream);
}