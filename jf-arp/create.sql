create table jf_product
(
  id text not null
    constraint jd_product_pk
      primary key,
  product_name text,
  price numeric
)
;

comment on table jf_product is '商品表'
;

comment on column jf_product.id is '主键'
;

comment on column jf_product.product_name is '商品名称'
;

comment on column jf_product.price is '商品价格'
;


