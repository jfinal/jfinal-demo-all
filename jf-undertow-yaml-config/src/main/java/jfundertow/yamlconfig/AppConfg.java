package jfundertow.yamlconfig;

import com.alibaba.fastjson.JSONArray;
import com.ax.framework.jfinal.db.Arp;
import com.ax.framework.jfinal.server.AppPropExt;
import com.ax.framework.jfinal.server.AppUndertowConfig;
import com.ax.framework.jfinal.server.AppUndertowServer;
import com.jfinal.config.*;
import com.jfinal.template.Engine;

public class AppConfg extends JFinalConfig {

    static AppPropExt apext = initAppext();

    public static void main(String[] args) {
        new AppUndertowServer(new AppUndertowConfig(AppConfg.class.getCanonicalName(), apext))
                .configWeb(builder -> {
                    builder.addServlet("/", "io.undertow.servlet.handlers.DefaultServlet");
                })
                .start();
    }

    static private AppPropExt initAppext() {
        String conf = System.getProperty("config");
        if (conf == null || conf.isEmpty()) {
            conf = "app.yml";
            System.out.println("未指定配置文件, 默认" + conf);
            // println("请指定配置文件");
            // System.exit(0)
        }
        return new AppPropExt(conf);
    }


    @Override
    public void configConstant(Constants me) {

    }

    @Override
    public void configRoute(Routes me) {
        me.add("/hello", HelloController2.class);
    }

    @Override
    public void configEngine(Engine me) {

    }

    @Override
    public void configPlugin(Plugins me) {
        //加载数据源
        JSONArray datasource = apext.getArray("datasource");
        if (datasource != null) Arp.INSTANCE.start(datasource);

    }

    @Override
    public void configInterceptor(Interceptors me) {

    }

    @Override
    public void configHandler(Handlers me) {

    }
}
