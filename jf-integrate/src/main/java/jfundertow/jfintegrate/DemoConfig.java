package jfundertow.jfintegrate;

import cn.hutool.core.io.resource.ClassPathResource;
import com.ax.framework.jfinal.db.Arp;
import com.jfinal.config.*;
import com.jfinal.template.Engine;

import java.io.InputStream;

public class DemoConfig extends JFinalConfig {

    public void configConstant(Constants me) {
        me.setDevMode(true);
    }

    public void configRoute(Routes me) {
        me.add("/hello", HelloController.class);
    }

    public void configEngine(Engine me) {
    }

    public void configPlugin(Plugins me) {
        //加载yml文件
        InputStream stream = new ClassPathResource("datasource.yml").getStream();
        //初始化数据源
        Arp.INSTANCE.init(stream);

    }

    public void configInterceptor(Interceptors me) {
    }

    public void configHandler(Handlers me) {
    }
}