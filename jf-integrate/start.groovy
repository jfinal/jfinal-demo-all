/*
  需要修改的只有
    classes_path  编译输出路径
    lib_dir       依赖jar包路径
    main_class    启动类的完整包路径
* */

def classes_path = "./target/classes"
def lib_dir = new File("./target//lib")
def main_class = "jfundertow.jfintegrate.App"

def command = '''@echo off
cmd /k "cd/d '''
command += classes_path
command += " && java -classpath "
lib_dir.eachFile() {
    file -> command += "../lib/${file.name};"
}
command += " "
command += main_class
command += "\""

def temdir = System.getProperty("java.io.tmpdir")
def start_bat = new File(temdir + "\\jf-all-demo.bat")
if (!start_bat.exists()) {
    start_bat.createNewFile()
}


start_bat.withWriter('gbk') {
    writer -> writer.writeLine command
}

println start_bat.absolutePath

Runtime.getRuntime().exec(start_bat.absolutePath)