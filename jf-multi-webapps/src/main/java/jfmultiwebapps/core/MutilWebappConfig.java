package jfmultiwebapps.core;

import com.jfinal.config.*;
import com.jfinal.template.Engine;
import jfmultiwebapps.webapp1.WebappConfig1;
import jfmultiwebapps.webapp2.Webapp2Config;

import java.util.ArrayList;

public class MutilWebappConfig extends JFinalConfig {

    ArrayList<WebappConfig> config_list = new ArrayList();


    /**
     * @author 张天笑
     * @time 2018/12/29 21:41
     */
    public MutilWebappConfig() {
        config_list.add(new WebappConfig1("/webapp1"));
        config_list.add(new Webapp2Config("/webapp2"));
    }


    @Override
    public void configConstant(Constants me) {
        config_list.forEach(it -> {
            it.configConstant(me);
        });
    }

    @Override
    public void configRoute(Routes me) {
        config_list.forEach(it -> {
            it.configRoute(me);
        });
    }

    @Override
    public void configEngine(Engine me) {
        config_list.forEach(it -> {
            it.configEngine(me);
        });
    }

    @Override
    public void configPlugin(Plugins me) {
        config_list.forEach(it -> {
            it.configPlugin(me);
        });
    }

    @Override
    public void configInterceptor(Interceptors me) {
        config_list.forEach(it -> {
            it.configInterceptor(me);
        });
    }

    @Override
    public void configHandler(Handlers me) {
        config_list.forEach(it -> {
            it.configHandler(me);
        });
    }

}
