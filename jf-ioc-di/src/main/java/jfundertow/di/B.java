package jfundertow.di;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.aop.Singleton;
import jfundertow.interceptor.TestInterceptor;

public class B {
    @Inject
    public A a;

    @Before(TestInterceptor.class)
    public void say_hello() {
        System.out.println("hello!");
    }
}
