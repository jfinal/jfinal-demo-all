package jfundertow.action;

import com.jfinal.aop.Aop;
import com.jfinal.config.*;
import com.jfinal.core.Controller;
import com.jfinal.core.ControllerFactory;
import com.jfinal.template.Engine;

public class DemoConfig extends JFinalConfig {

    public void configConstant(Constants me) {

        me.setControllerFactory(new ControllerFactory() {
            @Override
            public Controller getController(Class<? extends Controller> controllerClass) throws ReflectiveOperationException {
                return Aop.get(controllerClass);
            }
        });

        me.setDevMode(true);
    }

    public void configRoute(Routes me) {
        me.add("/hello", HelloController.class);
    }

    public void configEngine(Engine me) {
    }

    public void configPlugin(Plugins me) {
    }

    public void configInterceptor(Interceptors me) {
    }

    public void configHandler(Handlers me) {
    }
}