package jfundertow.action;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;

public class HelloController extends Controller {

    @Inject
    HelloService service;

    public void index() {
        renderJson(service.ids());
    }

    /*
    public void hot() {
        renderText("Hot!!!");
    }
    */
}