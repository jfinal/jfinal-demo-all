package jfundertow.websocket;

import com.jfinal.config.*;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.template.Engine;

public class DemoConfig extends JFinalConfig {

    public void configConstant(Constants me) {
        me.setDevMode(true);
    }

    public void configRoute(Routes me) {
        me.add("/hello", HelloController.class, "src/main/resources");
    }

    public void configEngine(Engine me) {
        // me.setBaseTemplatePath("src/main/resources");
        //  me.setToClassPathSourceFactory();
    }

    public void configPlugin(Plugins me) {
    }

    public void configInterceptor(Interceptors me) {
    }

    public void configHandler(Handlers me) {
        me.add(new UrlSkipHandler("/ws", false));
    }
}