package jfenjoy.demo1;

import com.jfinal.config.*;
import com.jfinal.template.Engine;
import com.jfinal.template.source.FileSourceFactory;

public class DemoConfig extends JFinalConfig {

    public void configConstant(Constants me) {
        me.setDevMode(true);
    }

    public void configRoute(Routes me) {
        me.add("/hello", HelloController.class);
    }

    public void configEngine(Engine me) {
        // 指定视图engine从硬盘目录读取文件
        me.setSourceFactory(new FileSourceFactory());
        // 不设置将从项目根目录读取文件
        //me.setBaseTemplatePath("C:\\dev_java\\project\\ic_project\\_jfinal_undertow\\jf-enjoy");
    }

    public void configPlugin(Plugins me) {
    }

    public void configInterceptor(Interceptors me) {
    }

    public void configHandler(Handlers me) {
    }
}